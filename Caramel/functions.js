var page = 0;

const hiddenOnFirstPage = document.getElementsByClassName("first-page-hidden");
const name = document.getElementsByClassName("name-header");
const hiddenOnLastPage = document.getElementsByClassName("last-page-hidden");
const dots = document.getElementsByClassName("dot");
const upButton = document.querySelector('.up-button');
const downButton = document.querySelector('.down-button');

const content = document.getElementsByClassName("content");
var inputListening = 1;

window.addEventListener('keydown', e => {
	if (inputListening) {
		inputListening = 0;
		setTimeout(() => {
			inputListening = 1;
		}, 2000);
		switch (e.code) {
			case "ArrowUp":
			if (window.scrollY == 0) {
				scrollUp();
			}
			break;
			case "ArrowDown":
			if ((window.innerHeight + window.scrollY) >= document.documentElement.scrollHeight) {
				scrollDown();
			}
			break;			
			default:
			/* don't do anything */
		}
	}
});

window.addEventListener('wheel', e => {
	if (inputListening) {
		inputListening = 0;
		setTimeout(() => {
			inputListening = 1;
		}, 2000);
		if (e.deltaY < 0 /* or some negative number so it doesn't fire on the slightest touch*/) {
	    // scrolled up
	    if (window.scrollY == 0) {
	    	scrollUp();
	    }
	  } else if (e.deltaY > 0 /* or some positive number so it doesn't fire on the slightest touch*/) {
	    // scrolled down
	    if ((window.innerHeight + window.scrollY) >= document.documentElement.scrollHeight) {
	    	scrollDown();
	    }
	  }
	}
});


upButton.addEventListener('click', e => {
	scrollUp();
});
downButton.addEventListener('click', e => {
	scrollDown();
});

function scrollUp() {
	/* leaving last page */
	if (page === 4) {
		for (let hiddenElements of hiddenOnLastPage) {
			hiddenElements.style.opacity = 1;
		}
	}

	page = page - 1;
	/* already on first page */
	if (page < 0) page = 0;

	/* new page */
	else {
		content[page + 1].classList.add("anim-leave-page");
			setTimeout(() => {
				dots[page + 1].classList.remove("current-page");
				dots[page].classList.add("current-page");
				content[page + 1].classList.remove("current-content");
				content[page].classList.add("current-content");
			}, 2000);

		if (page === 0) {
			for (let hiddenElements of hiddenOnFirstPage) {
				hiddenElements.style.opacity = 0;
			}
			for (let n of name) {
				n.style.visibility = "hidden";
			}
		}
	}

	window.scrollTo(0,0);
}
function scrollDown() {
	/* leaving first page */
	if (page === 0) {
		content[page].classList.add("anim-leave-first");

		setTimeout(() => {
			content[page].classList.remove("anim-leave-first");

			for (let hiddenElements of hiddenOnFirstPage) {
				hiddenElements.style.opacity = 1;
			}
			for (let n of name) {
				n.style.visibility = "visible";
			}
			page = 1;

			dots[page - 1].classList.remove("current-page");
			dots[page].classList.add("current-page");
			content[page - 1].classList.remove("current-content");
			content[page].classList.add("current-content");
		}, 2000);
	}
	else {
		for (let hiddenElements of hiddenOnFirstPage) {
			hiddenElements.style.opacity = 1;
		}
		for (let n of name) {
			n.style.visibility = "visible";
		}
		page = page + 1;
		/* already on last page */
		if (page > 4) page = 4;

		/* new page */
		else {
			content[page - 1].classList.add("anim-leave-page");
			setTimeout(() => {
				content[page - 1].classList.remove("anim-leave-page");
				dots[page - 1].classList.remove("current-page");
				dots[page].classList.add("current-page");
				content[page - 1].classList.remove("current-content");
				content[page].classList.add("current-content");
			}, 2000);

			if (page === 4) {
				for (let hiddenElements of hiddenOnLastPage) {
					hiddenElements.style.opacity = 0;
				}
			}
		}
	}

	window.scrollTo(0,0);
}


const carouselList = document.querySelector('.carousel__list');
const carouselItems = document.querySelectorAll('.carousel__item');
const elems = Array.from(carouselItems);

carouselList.addEventListener('click', function (event) {
	var newActive = event.target;
	console.log("target: ");
	console.log(event.target);
	var isItem = newActive.closest('.carousel__item');
	console.log("isItem: ");
	console.log(isItem);

	if (!isItem || newActive.classList.contains('carousel__item_active')) {
		return;
	};

	update(isItem);
});

const update = function(newActive) {
	const newActivePos = newActive.dataset.pos;

	const current = elems.find((elem) => elem.dataset.pos == 0);
	const prev = elems.find((elem) => elem.dataset.pos == -1);
	const next = elems.find((elem) => elem.dataset.pos == 1);
	const first = elems.find((elem) => elem.dataset.pos == -2);
	const last = elems.find((elem) => elem.dataset.pos == 2);

	current.classList.remove('carousel__item_active');

	[current, prev, next, first, last].forEach(item => {
		var itemPos = item.dataset.pos;

		item.dataset.pos = getPos(itemPos, newActivePos)
	});
};

const getPos = function (current, active) {
	if (Math.abs(active) == 1) {
		const diff = current - active;

		if (Math.abs(current - active) > 2) {
			return -current
		}

		return diff;
	}
	else if (Math.abs(active) == 2) {
		const diff = current - active;

		switch (diff) {
			case -3:
			return 2;
			case -4:
			return 1;
			case 3:
			return -2;
			case 4:
			return -1;
			default:
			return diff;
		}		
	}
	else return current;
}


const losangeItems = document.querySelectorAll('.losange__item');
const elemsLo = Array.from(losangeItems);
const tool = document.getElementById("t1");
const toolInfo = document.getElementById("t2");

losangeItems.forEach(item => item.addEventListener('click', function (event) {
	var newActive = item;

	if (newActive.classList.contains('losange__item_active')) {
		return;
	};

	updateLo(newActive);
}));

const updateLo = function(newActive) {
	const newActivePos = newActive.dataset.pos;

	const current = elemsLo.find((elem) => elem.dataset.pos == 0);
	const prev = elemsLo.find((elem) => elem.dataset.pos == -1);
	const next = elemsLo.find((elem) => elem.dataset.pos == 1);
	const first = elemsLo.find((elem) => elem.dataset.pos == -2);
	const last = elemsLo.find((elem) => elem.dataset.pos == 2);
	const third = elemsLo.find((elem) => elem.dataset.pos == 3);

	current.classList.remove('losange__item_active');

	[current, prev, next, first, last, third].forEach(item => {
		var itemPos = item.dataset.pos;

		item.dataset.pos = getPosLo(itemPos, newActivePos);
		if (item.dataset.pos == 0) {
			switch (item.children[0].alt) {
				case 'Angular':
				tool.innerHTML = "Angular";
				toolInfo.innerHTML = "One year of experience through project and internship.";
				break;
				case 'Figma':
				tool.innerHTML = "Figma";
				toolInfo.innerHTML = "Systematic for each project design.";
				break;
				case 'Git':
				tool.innerHTML = "Git";
				toolInfo.innerHTML = "Systematic for each project.";
				break;
				case 'Visual Studio':
				tool.innerHTML = "Visual Studio";
				toolInfo.innerHTML = "Used in web projects and work force internship.";
				break;
				case 'Bootstrap':
				tool.innerHTML = "Bootstrap";
				toolInfo.innerHTML = "Employed on every big project.";
				break;
				case 'Android Studio':
				tool.innerHTML = "Android Studio";
				toolInfo.innerHTML = "One year of experience through school projects.";
				break;
			}
		}
	});
};

const getPosLo = function (current, active) {
	const diff = current - active;

	switch (diff) {
		case -3:
		return 3;
		case -4:
		return 2;
		case -5:
		return 1;
		case 5:
		return -1;
		case 4:
		return -2;
		default:
		return diff;
	}
}